---
title: Contribute to CalyxOS
nav_title: Contribute
---

## Donate

Your tax-deductible donation to Calyx Institute will fund our development of CalyxOS.

The Calyx Institute's mission is to educate the public about privacy in digital communications and to develop tools that anyone can use to build "privacy by design" into their internet access. By developing encryption and anonymity tools that can help users maintain their privacy, we hope to make online security easier and more accessible for everyone online.

<a class="btn btn-outline-secondary" href="https://members.calyxinstitute.org/donate">Donate Now</a>

## Development

CalyxOS is Free and Open Source Software.

All development of CalyxOS takes place out in the open on public source code repositories. We would love to have you get involved.

Our source code is available for review at [gitlab.com/CalyxOS](https://gitlab.com/CalyxOS).

Development is done on our [Gerrit Code Review Instance](https://review.calyxos.org/).

You can get started by following our HOWTO for [creating a CalyxOS development environment](https://gitlab.com/CalyxOS/calyxos/wikis/Getting-Started).

Then, check out [list of open issues](https://gitlab.com/CalyxOS/calyxos/issues/).

## Code of Conduct
See: [Code of Conduct](code-of-conduct)
